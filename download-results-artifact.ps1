﻿param ($jobId, $outFile='Results/results.xml')

$reportUrl = "https://sshiff22.gitlab.io/-/gitlab-pipeline-tosca-demo/-/jobs/$jobId/artifacts/Results/results.xml"
#Write-Host "Link to the file: " $reportUrl

Invoke-WebRequest -Uri $reportUrl -OutFile $outFile
