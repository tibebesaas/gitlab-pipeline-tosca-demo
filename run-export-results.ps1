
$TCShellExe="$Env:COMMANDER_HOME\TCShell.exe"
$workspace="$Env:TRICENTIS_PROJECTS\Tosca_Workspaces\DEX_Workspace\DEX_Workspace.tws"
$workspaceFolder=split-path -Parent $workspace
$user="`"Tosca`""
$password="`"tosca`""

if ($env:CI_PROJECT_DIR) {$tscFileLocation=$env:CI_PROJECT_DIR}
else  {$tscFileLocation = split-path -parent $MyInvocation.MyCommand.Definition}

write-host "Running command: `n& `"$TCShellExe`" -workspace `"$workspace`" -login $user $password `"$tscFileLocation\$tscFile`"`n"
write-host "Current folder is: " $pwd

#write-host "cmd"
&$TCShellExe -workspace $workspace -login $user $password $tscFileLocation\$tscFile

#write-host Change forlder to: $tscFileLocation
#cd "$workspaceFolder"
#write-host "Current folder is: " $pwd
